package ru.appline.logic;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Model implements Serializable{
	
	private static final Model instance = new Model();
	
	private final Map<Integer, User> model;
	
	public static Model getinstance() {
		return instance;
	}

	private Model() {
		model = new HashMap<>();
		
		model.put(1, new User("1", "2", 3));
		model.put(2, new User("4", "5", 6));
		model.put(3, new User("7", "8", 9));
		model.put(4, new User("�", "�", 0));
	}
	
	public void add (User user, int id) {
		model.put(id, user);
	}
	
	public Map<Integer, User> getFromList() {
		return model;
	}
}
