package ru.appline;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ru.appline.logic.Model;
import ru.appline.logic.User;


@WebServlet(urlPatterns = "/put")
public class ServletPut extends HttpServlet {
	Model model = Model.getinstance();
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
	response.setContentType("text/html; charset=UTF-8");
	PrintWriter pw = response.getWriter();

	int id = Integer.parseInt(request.getParameter("id"));
	String name = request.getParameter("name");
	String surname = request.getParameter("surname");
	double salary = Double.parseDouble(request.getParameter("salary"));
	
	User user = new User(name, surname, salary);
	model.add(user, id);
	
	pw.print("<html>" +
			"<h3>������������ ID: " + id + " ������� ������� ��: <h3>" + 
			"������������ " + name + " " + surname + " � ���������: " + salary + ".<br/>" + 
			"<a href=\"index.jsp\">�����</a>" + "</html>");	
	
	}
}
