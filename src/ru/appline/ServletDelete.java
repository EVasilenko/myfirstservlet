package ru.appline;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ru.appline.logic.Model;
import ru.appline.logic.User;

@WebServlet(urlPatterns = "/delete")
public class ServletDelete extends HttpServlet {
	Model model = Model.getinstance();

		
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter pw = response.getWriter();
		
		int id = Integer.parseInt(request.getParameter("id"));
		
		if(id==0) {

			Iterator<Map.Entry<Integer, User>> iterator = model.getFromList().entrySet().iterator();
			
			 while(iterator.hasNext()) {
		            Map.Entry<Integer, User> entry = (Map.Entry<Integer, User>) iterator.next();
	                iterator.remove();
		            }
			 
				pw.print("<html>" + 
						"<h3>������� ��� ������������</h3><br/>" +
						"<a href=\"index.jsp\">�����</a>" + "</html>");
			
		} else if (id > 0) {
			if (id > model.getFromList().size()) {
				pw.print("<html>" +
						"<h3>������ ������������ ���!<h3>" + 
						"<a href=\"index.jsp\">�����</a>" + "</html>");
			} else {
				model.getFromList().remove(id);
//				model.delete(id);
				
				pw.print("<html>" +
						"<h3>����������� ������������ � ID=</h3>" + id + " ������� ������! <h3>" + 
						"<a href=\"index.jsp\">�����</a>" + "</html>");
				
			}
			
		} else {
			pw.print("<html>" +
					"<h3>ID ������ ���� ������ 0.<h3>" + 
					"<a href=\"index.jsp\">�����</a>" + "</html>");
		}
			
		}
//@Override
//		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
//	response.setContentType("text/html; charset=UTF-8");
//		}
}
